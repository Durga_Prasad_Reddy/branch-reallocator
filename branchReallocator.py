import csv 
import numpy as np
import itertools
import math

# Give the CSV file name containing the details of students in studentFileName variable
# Give the CSV file name containing the details of branches in branchFileName variable

studentFileName = "batch2018data.txt"  
branchFileName = "branch_info.txt"

# Each element of student_info_rows array represents one student
# Each element of branch_info_rows array represents one branch
# temporaryStrengthDictionary is a Dictionary used to keep track of intermediate changes of class strengths during the shiftings

student_info_rows = []
branch_info_rows = []
temporaryStrengthDictionary = {}
sanctionedStrengthDictionary = {}

# Reading into the file

with open(studentFileName, 'r') as csvfile: 
    csvreader = csv.reader(csvfile) 
    for student_info_row in csvreader: 
        student_info_rows.append(student_info_row)
    no_of_students = csvreader.line_num  

with open(branchFileName, 'r') as readFile:
	branch_info_reader = csv.reader(readFile)

	for branch_info_row in branch_info_reader:
		branch_info_rows.append(branch_info_row)

	no_of_branches = branch_info_reader.line_num

#------------------------------------------------------------------------------------------------------------------------------
# Two classes are used in the entire code. 1 for student details and 1 for branch details
# list1 is 1 record in the studentInfo file. It contains all the details about 1 student in the following order

class Student:
	def __init__(self, list1):
		self.roll_no = list1[0]
		self.JEE_rank = list1[1]
		self.current_branch = list1[2]
		self.GPA = list1[3]
		self.preference_branch = [list1[4], list1[5], list1[6]]

# The ranges of each department class strength is calculated here
# For minPossibleStrength, We need to allow 1 student to move out of the branch so the appropriate code is written

class Branch:
	def __init__(self,list2):
		self.branchName = list2[0]
		self.existingStrength = list2[1]
		self.sanctionedStrength = list2[2]
		self.minPossibleStrength = min(math.ceil(int(self.existingStrength) * 0.9), int(self.existingStrength) - 1)
		self.maxPossibleStrength = float(int(self.sanctionedStrength) * 1.1)


#------------------------------------------------------------------------------------------------------------------------------

# Initialising the temporaryStrengthDictionary to the values given in the data
for i in range(0,4):
	branchObject = Branch(branch_info_rows[i])
	temporaryStrengthDictionary[branchObject.branchName] = branchObject.existingStrength
	sanctionedStrengthDictionary[branchObject.branchName] = branchObject.sanctionedStrength

writeToReport = open("Report.md", 'w')
writeToReport.write('The Sanctioned Strength of each branch is as follows - ' + "\n")
writeToReport.write(str(sanctionedStrengthDictionary) + "\n\n")
writeToReport.write('The class strengths of each branch before reallocation is as follows - ' + "\n")
writeToReport.write(str(temporaryStrengthDictionary) + "\n\n")
#------------------------------------------------------------------------------------------------------------------------------

# Sorting the Data in Descending order of GPA. If there is a GPA clash, student with better JEE rank is put first

def sortRank(val):
	return val[1]

student_info_rows.sort(key = sortRank)

def sortGPA(val):
	return val[3]

student_info_rows.sort(key = sortGPA, reverse = True)

#------------------------------------------------------------------------------------------------------------------------------

# The changeStrength function takes 2 arguments, 1st argument will determine which student and 2nd argument will determine which preference branch
# When those 2 arguments are passed, this function decrements that student's current branch strength by 1 and increments his alloted preference branch strength by 1

def changeStrength(val1, val2):
	currentStudent = Student(student_info_rows[val1])

	if val2 < 3 and currentStudent.preference_branch[val2] != '':
		temporaryStrengthDictionary[currentStudent.current_branch] = int(temporaryStrengthDictionary[currentStudent.current_branch]) - 1
		temporaryStrengthDictionary[currentStudent.preference_branch[val2]] = int(temporaryStrengthDictionary[currentStudent.preference_branch[val2]]) + 1


#------------------------------------------------------------------------------------------------------------------------------

# checkStrengthRule function takes 3 arguments, dict is the dictionary which has the class strength of each branch
# If currentBRANCH and PreferenceBRANCH of a particular student are passed to this function, it returns Satisfied if he got into the branch
# Or if he is rejected, this function will return the reject reason to that particular branch
# If a user doesn't want to check a particular student's status, he can simple pass None for the 2nd and 3rd parameters

def checkStrengthRule(dict, currentBRANCH, PreferenceBRANCH):
	ruleSatisfiedDictionary = {}
	for i in range(no_of_branches):
		branchObject = Branch(branch_info_rows[i])
		ruleSatisfiedDictionary[branchObject.branchName] = 'null'


	for x in range(0, no_of_branches):
		temp_branch = Branch(branch_info_rows[x])
		if int(dict[temp_branch.branchName]) >= temp_branch.minPossibleStrength:
			if int(dict[temp_branch.branchName]) <= temp_branch.maxPossibleStrength:		
				ruleSatisfiedDictionary[temp_branch.branchName] = 'Satisfied'

			else:
				ruleSatisfiedDictionary[temp_branch.branchName] = 'preference branch maxed out'

		else:
			ruleSatisfiedDictionary[temp_branch.branchName] = 'current branch bottomed out'

	if all(value == 'Satisfied' for value in ruleSatisfiedDictionary.values()) == True:
		return 'Satisfied'

	else:
		for i in range(no_of_branches):
			branchObject = Branch(branch_info_rows[i])
			if PreferenceBRANCH != None:
				if ruleSatisfiedDictionary[PreferenceBRANCH] != 'Satisfied':
					return ruleSatisfiedDictionary[PreferenceBRANCH]

				else:
					return ruleSatisfiedDictionary[currentBRANCH]

			else:
				return 'Unsatisfied'


#------------------------------------------------------------------------------------------------------------------------------

# checkRule5 function checks whether in a particular allocation Rule 5 is violated or not
# It takes 1 argument which is the allocation we need to check in the form of a matrix

def checkRule5(objectMatrix):

	rule5CheckingDictionary = {}

	for i in range(0, no_of_branches):
		branchObject = Branch(branch_info_rows[i])
		rule5CheckingDictionary[branchObject.branchName] = 0

	rule5CheckingDictionary[''] = 100
	p = 0
	flagToBreak = 0

	while p < no_of_students:

		rule5TempStudent = Student(student_info_rows[p])
		q = 0

		while q < no_of_branches - 1:
			if objectMatrix[p][q] == 1:

				if float(rule5CheckingDictionary[rule5TempStudent.preference_branch[q]]) > float(rule5TempStudent.GPA):
					flagToBreak = flagToBreak + 1
				break

			elif objectMatrix[p][q] == 0:
				if rule5CheckingDictionary[rule5TempStudent.preference_branch[q]] == 0:
					rule5CheckingDictionary[rule5TempStudent.preference_branch[q]] = rule5TempStudent.GPA
			q = q + 1
		p = p + 1

	if flagToBreak != 0:
		return 'Unsatisfied'
		
	else:
		return 'Satisfied'


#------------------------------------------------------------------------------------------------------------------------------

print('Please type Y if you want to include Rule 5 or type N if you dont want to include Rule 5')
inputRule5 = input()
print('Please be patient your output will be generated soon...')

x = range(no_of_branches)
flag4 = 0

for q in itertools.product(x, repeat = no_of_students):
	flag3 = 0
	flag4 = flag4 + 1
	outputList = []
	for p in q:

		studentTemp = Student(student_info_rows[flag3])
		if p < 3 and studentTemp.preference_branch[p] == '':
			outputList.append('doesnt exist')

		else:
			outputList.append('exist')

		flag3 = flag3 + 1

	flag3 = 0

	if all(tempString == 'exist' for tempString in outputList):
		outputMatrix = np.zeros(shape = (no_of_students, 4))

		for j in q:
			outputMatrix[flag3][j] = 1.0
			flag3 = flag3 + 1
		temp1 = -1

		for i in range(0, no_of_students):
			temp1 = temp1 + 1
			temp2 = 0
			for j in outputMatrix[i]:	
				if j == 1.0:
					break

				else:
					temp2 = temp2 + 1

			changeStrength(temp1, temp2)

		if checkStrengthRule(temporaryStrengthDictionary, None, None) == 'Satisfied':
			if inputRule5 == 'Y':
				if checkRule5(outputMatrix) == 'Satisfied':
					print('Rule 5 checked')
					break
			
			else:
				break

	
		for i in range(no_of_branches):
			branchObject = Branch(branch_info_rows[i])
			temporaryStrengthDictionary[branchObject.branchName] = int(branchObject.existingStrength)



print(temporaryStrengthDictionary)

print(outputMatrix)

reportGeneratingMatrix = outputMatrix
reportAssistingDictionary = temporaryStrengthDictionary
writeToReport.write('The Final Class Strengths of Each branch after reallocation is as follows - ' + "\n")
writeToReport.write(str(temporaryStrengthDictionary) + "\n\n")

for p in range(no_of_students):

	studentObject = Student(student_info_rows[p])
	writeToReport.write('Student ' + studentObject.roll_no + "\n")
	for q in range(no_of_branches):
		if reportGeneratingMatrix[p][q] == 1:
			break

	if q != 0:
		for y in range(0, q):
			if studentObject.preference_branch[y] != '':
				reportGeneratingMatrix[p][q] = 0
				reportGeneratingMatrix[p][y] = 1

				if q < 3:
					reportAssistingDictionary[studentObject.preference_branch[q]] = reportAssistingDictionary[studentObject.preference_branch[q]] - 1
					
				else:
					reportAssistingDictionary[studentObject.current_branch] = reportAssistingDictionary[studentObject.current_branch] - 1
					
				reportAssistingDictionary[studentObject.preference_branch[y]] = reportAssistingDictionary[studentObject.preference_branch[y]] + 1
				strengthRuleValue = checkStrengthRule(reportAssistingDictionary, studentObject.current_branch, studentObject.preference_branch[y])
				rule5Value = checkRule5(reportGeneratingMatrix)

				if strengthRuleValue != 'Satisfied':
					writeToReport.write('- Got rejected to Preference ' + str(y + 1) + ' which is ' + studentObject.preference_branch[y] + ' because ' + strengthRuleValue + "\n")

				else:
					writeToReport.write('- Got rejected to Preference ' + str(y + 1) + ' which is ' + studentObject.preference_branch[y] + ' because ' + 'a higher GPA student got rejected to this branch before ' + "\n")

				reportGeneratingMatrix[p][q] = 1
				reportGeneratingMatrix[p][y] = 0

				reportAssistingDictionary = temporaryStrengthDictionary

		if q != 3:
			writeToReport.write('- Moved out of his current branch which is ' + studentObject.current_branch + "\n")
			writeToReport.write('- Got his Preference branch ' + str(q + 1) + ' which is ' + studentObject.preference_branch[q])

		else:
			writeToReport.write('- Stays in his current branch which is ' + studentObject.current_branch)

	if q == 0:
		writeToReport.write('- Moved out of his current branch which is ' + studentObject.current_branch + "\n")
		writeToReport.write('- Got his Preference branch ' + str(q + 1) + ' which is ' + studentObject.preference_branch[q])

	writeToReport.write("\n\n")

